# KHK Independent

*A Diara OÜ project*

This is the independent project for KHK: Payroll calculator application with Kohana / Bootstrap frameworks.

## Project summary

*Fill this out for assessment*

* **Author(s):** <your-name(s)> <your-email>
* **GitHub URL:** <change-me>
* **Live demo URL:** <change-me>

## Mentors

Ask technical questions from...

* Ando Roots <ando.roots@diara.ee>
* Henno Täht <henno.taht@diara.ee>